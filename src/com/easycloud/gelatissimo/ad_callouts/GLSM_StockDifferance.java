package com.easycloud.gelatissimo.ad_callouts;

import java.math.BigDecimal;

import javax.servlet.ServletException;

import org.openbravo.erpCommon.ad_callouts.SimpleCallout;

public class GLSM_StockDifferance extends SimpleCallout {

	@Override
	protected void execute(CalloutInfo info) throws ServletException {
		//String mvqnt = info.getStringParameter("inpglsmMovementqty");
		BigDecimal physicalqtycount = info.getBigDecimalParameter("inpphyqnty");
		BigDecimal systemqtycount = info.getBigDecimalParameter("inpsystemqty");
		
		BigDecimal diff = systemqtycount.subtract(physicalqtycount);
		info.addResult("inpmovementqty", diff.toString());
		System.out.println(diff);

	}

}
