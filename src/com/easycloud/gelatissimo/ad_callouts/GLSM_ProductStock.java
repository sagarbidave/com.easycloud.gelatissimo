package com.easycloud.gelatissimo.ad_callouts;

import java.math.BigDecimal;
import java.util.List;

import javax.servlet.ServletException;

import org.hibernate.Query;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.ad_callouts.SimpleCallout;
import org.openbravo.model.common.plm.Product;
import org.openbravo.model.materialmgmt.onhandquantity.StorageDetail;

public class GLSM_ProductStock extends SimpleCallout {

	@Override
	protected void execute(CalloutInfo info) throws ServletException {
		String product = info.getStringParameter("inpmProductId");
		Product pobj = OBDal.getInstance().get(Product.class, product);
		System.out.println(pobj);
		String hql = "select e from MaterialMgmtStorageDetail e where e.product = '" + pobj.getId() + "'";
		Query stock = OBDal.getInstance().getSession().createQuery(hql);

		if (!stock.list().isEmpty()) {
			List<StorageDetail> qtobj = stock.list();
			for(StorageDetail obj : qtobj) {
			//StorageDetail strobj = OBDal.getInstance().get(StorageDetail.class, obj.equals(pobj));
			
			info.addResult("inpsystemqty", obj.getQuantityOnHand());
			info.addResult("inpcUomId", obj.getProduct().getUOM().getId());
			//info.addResult("inpproductunit", obj.getProduct().getUOM().getName());
			}
		}
	}
}
